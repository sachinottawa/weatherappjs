import Model from "./model.js";

class Controller {
    constructor(){
        this.model = new Model()
    }
    async getTemperatureInF(){
        const weather = await this.model.getWeather()
        const current = weather.current_weather
        const tempInC = current.temperature
        const tempInF = (tempInC * 9/5) + 32
        return tempInF
    }
}

export default Controller