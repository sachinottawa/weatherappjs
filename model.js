class Model{
    constructor(){

    }
    async getWeather(){
        const data = await fetch(`https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&current_weather=true&hourly=temperature_2m`)
        const weather = await data.json()
        return weather
    }
}

export default Model

